#!/usr/bin/env python3

def dzip(n, elements):
    """
    Zip a list of elements into n-tuples.

    - n : integer.
    - elements : generator or list of anything.
    - return : generator of tuples of anything.

    Note that if `elements` is a generator,
    `elements` cannot be reused
    after calling this function
    because the generator will be consumed.

    If `elements` is needed after this function,
    save it calling `itertools.tee(elements)` or `list(elements)`
    before calling this function;
    or regenerate it.
    """
    from itertools import tee, islice

    n = max(0, n)

    return zip(*[islice(g, i, None) for i, g in zip(range(n), tee(elements, n))])

def main():
    import sys

    n = int(sys.argv[1])

    for tuples in dzip(n, (line.strip() for line in sys.stdin)):
        print(*tuples)

if __name__ == "__main__":
    main()
